uniform mat4 g_WorldViewProjectionMatrix;
uniform mat4 g_WorldMatrix;

attribute vec3 inNormal;
attribute vec3 inPosition;

varying vec3 normal;
varying vec4 position;
varying vec4 vLightDir;


// JME3 lights in world space
void lightComputeDir(in vec3 worldPos, in vec4 color, in vec4 position, out vec4 lightDir){
    float posLight = step(0.5, color.w);
    vec3 tempVec = position.xyz * sign(posLight - 0.5) - (worldPos * posLight);
    lightVec.xyz = tempVec;  
    float dist = length(tempVec);
    lightDir.w = clamp(1.0 - position.w * dist * posLight, 0.0, 1.0);
    lightDir.xyz = tempVec / vec3(dist);
}

void main()
{
 	normal = normalize(inNormal);
 	position = g_WorldMatrix * vec4(inPosition, 0.0);

    lightComputeDir(wvPosition, lightColor, wvLightPos, vLightDir);

    gl_Position = g_WorldViewProjectionMatrix * vec4(inPosition, 1);
}
