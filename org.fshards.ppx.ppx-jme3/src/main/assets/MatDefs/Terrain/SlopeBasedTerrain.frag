uniform float m_Shininess;
uniform vec4 g_LightDirection;

varying vec4 AmbientSum;
varying vec4 DiffuseSum;
varying vec4 SpecularSum;

varying vec3 normal;
varying vec2 texCoord;
varying vec4 position;
varying vec3 vnPosition;
varying vec3 vViewDir;
varying vec4 vLightDir;
varying vec4 vnLightDir;
varying vec3 lightVec;


void main(void)
{
	vec4 color = vec4(0, 0.7, 0, 1.0);

    vec4 lightDir = vLightDir;
    vec3 normal = normalize(normal);
    lightDir.xyz = normalize(lightDir.xyz);


	float diffuseFactor = max(0.0, dot(normal, lightDir));
	
	if (normal.x == 0) {
		color.x = 1.0;
	}
        
	gl_FragColor = color / 2.0 * diffuseFactor + color / 2.0;
}

