package org.fshards.ppx.jme3.deprecated;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.fshards.ppx.jme3.screens.Inventory;
import org.fshards.ppx.jme3.scripts.JSLoader;
import org.fshards.ppx.jme3.scripts.ScreenManager;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;

import tonegod.gui.core.Screen;

/**
 * Main application
 *
 * @author gabor.csepregi
 *
 */
public class App extends SimpleApplication
{

	private static final String PPX_CONFIG_ASSETS_FOLDER = "ppx.config.assetsFolder";

	public static void main( final String[ ] args )
	{
		final App app = new App( );
		app.start( );
	}

	private ScriptEngine	scriptEngine;
	private ScreenManager	screenManager;
	private Screen			screen;

	@Override
	public void simpleInitApp( )
	{
		final String assetsFolder = System.getenv( App.PPX_CONFIG_ASSETS_FOLDER ) == null
				? System.getProperty( App.PPX_CONFIG_ASSETS_FOLDER ) : System.getenv( App.PPX_CONFIG_ASSETS_FOLDER );

		this.assetManager.registerLocator( assetsFolder, FileLocator.class );

		this.screen = new Screen( this, "Interface/tonegod/Kenney/style_map.gui.xml" );
		this.guiNode.addControl( this.screen );
		System.out.println( "ScriptsFolder: " + assetsFolder );
		this.scriptEngine = new ScriptEngineManager( ).getEngineByMimeType( "application/javascript" );
		try
		{
			this.assetManager.registerLoader( JSLoader.class, "js" );

			final String asset = ( String ) this.assetManager.loadAsset( "scripts/test.js" );

			this.scriptEngine.eval( asset );
			final Invocable inv = ( Invocable ) this.scriptEngine;
			this.screenManager = inv.getInterface( ScreenManager.class );
			this.screenManager.testInterface( "Hello world" );
		}
		catch ( final ScriptException e )
		{
			e.printStackTrace( );
		}

		this.getStateManager( ).attach( new Inventory( this.screen ) );
	}

}