drop table if exists perlin_noise;
create table perlin_noise (
	x		float,
	y		float,
	z		float,
	noise	float
);