/**
 * Copyright (c) 2016, 4Shards Entertainment Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.fshards.ppx.mcubes;

import java.util.ArrayList;
import java.util.List;

import org.fshards.ppx.jme3.ShaderUtils;

import com.jme3.math.Vector3f;

public class MCubes
{

	public static List< Triangle > polygonise( final GridCell grid, final float isolevel )
	{
		int cubeindex = 0;
		final Vector3f[ ] vertlist = new Vector3f[ 12 ];
		final List< Triangle > result = new ArrayList< Triangle >( );

		/*
		 * Determine the index into the edge table which tells us which vertices
		 * are inside of the surface
		 */
		if ( grid.val[ 0 ] < isolevel )
		{
			cubeindex |= 1;
		}
		if ( grid.val[ 1 ] < isolevel )
		{
			cubeindex |= 2;
		}
		if ( grid.val[ 2 ] < isolevel )
		{
			cubeindex |= 4;
		}
		if ( grid.val[ 3 ] < isolevel )
		{
			cubeindex |= 8;
		}
		if ( grid.val[ 4 ] < isolevel )
		{
			cubeindex |= 16;
		}
		if ( grid.val[ 5 ] < isolevel )
		{
			cubeindex |= 32;
		}
		if ( grid.val[ 6 ] < isolevel )
		{
			cubeindex |= 64;
		}
		if ( grid.val[ 7 ] < isolevel )
		{
			cubeindex |= 128;
		}

		/* Cube is entirely in/out of the surface */
		if ( MCubesHelper.edgeTable[ cubeindex ] == 0 )
		{
			return result;
		}

		/* Find the vertices where the surface intersects the cube */
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 1 ) > 0 )
		{
			vertlist[ 0 ] = MCubes.VertexInterp( isolevel, grid.p[ 0 ], grid.p[ 1 ], grid.val[ 0 ], grid.val[ 1 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 2 ) > 0 )
		{
			vertlist[ 1 ] = MCubes.VertexInterp( isolevel, grid.p[ 1 ], grid.p[ 2 ], grid.val[ 1 ], grid.val[ 2 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 4 ) > 0 )
		{
			vertlist[ 2 ] = MCubes.VertexInterp( isolevel, grid.p[ 2 ], grid.p[ 3 ], grid.val[ 2 ], grid.val[ 3 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 8 ) > 0 )
		{
			vertlist[ 3 ] = MCubes.VertexInterp( isolevel, grid.p[ 3 ], grid.p[ 0 ], grid.val[ 3 ], grid.val[ 0 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 16 ) > 0 )
		{
			vertlist[ 4 ] = MCubes.VertexInterp( isolevel, grid.p[ 4 ], grid.p[ 5 ], grid.val[ 4 ], grid.val[ 5 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 32 ) > 0 )
		{
			vertlist[ 5 ] = MCubes.VertexInterp( isolevel, grid.p[ 5 ], grid.p[ 6 ], grid.val[ 5 ], grid.val[ 6 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 64 ) > 0 )
		{
			vertlist[ 6 ] = MCubes.VertexInterp( isolevel, grid.p[ 6 ], grid.p[ 7 ], grid.val[ 6 ], grid.val[ 7 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 128 ) > 0 )
		{
			vertlist[ 7 ] = MCubes.VertexInterp( isolevel, grid.p[ 7 ], grid.p[ 4 ], grid.val[ 7 ], grid.val[ 4 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 256 ) > 0 )
		{
			vertlist[ 8 ] = MCubes.VertexInterp( isolevel, grid.p[ 0 ], grid.p[ 4 ], grid.val[ 0 ], grid.val[ 4 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 512 ) > 0 )
		{
			vertlist[ 9 ] = MCubes.VertexInterp( isolevel, grid.p[ 1 ], grid.p[ 5 ], grid.val[ 1 ], grid.val[ 5 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 1024 ) > 0 )
		{
			vertlist[ 10 ] = MCubes.VertexInterp( isolevel, grid.p[ 2 ], grid.p[ 6 ], grid.val[ 2 ], grid.val[ 6 ] );
		}
		if ( ( MCubesHelper.edgeTable[ cubeindex ] & 2048 ) > 0 )
		{
			vertlist[ 11 ] = MCubes.VertexInterp( isolevel, grid.p[ 3 ], grid.p[ 7 ], grid.val[ 3 ], grid.val[ 7 ] );
		}

		/* Create the triangle */
		for ( int i = 0; MCubesHelper.triTable[ cubeindex ][ i ] != -1; i += 3 )
		{
			final Triangle triangle = new Triangle( );
			triangle.p[ 0 ] = vertlist[ MCubesHelper.triTable[ cubeindex ][ i ] ];
			triangle.p[ 1 ] = vertlist[ MCubesHelper.triTable[ cubeindex ][ i + 1 ] ];
			triangle.p[ 2 ] = vertlist[ MCubesHelper.triTable[ cubeindex ][ i + 2 ] ];
			result.add( triangle );
		}

		return result;
	}

	/*
	 * Linearly interpolate the position where an isosurface cuts an edge
	 * between two vertices, each with their own scalar value
	 */
	public static Vector3f VertexInterp( final float isolevel, final Vector3f p1, final Vector3f p2, final float valp1, final float valp2 )
	{
		float mu;
		final Vector3f p = new Vector3f( );

		if ( ShaderUtils.abs( isolevel - valp1 ) < 0.00001f )
		{
			return p1;
		}
		if ( ShaderUtils.abs( isolevel - valp2 ) < 0.00001f )
		{
			return p2;
		}
		if ( ShaderUtils.abs( valp1 - valp2 ) < 0.00001f )
		{
			return p1;
		}
		mu = ( isolevel - valp1 ) / ( valp2 - valp1 );
		p.x = p1.x + mu * ( p2.x - p1.x );
		p.y = p1.y + mu * ( p2.y - p1.y );
		p.z = p1.z + mu * ( p2.z - p1.z );

		return p;
	}

}
