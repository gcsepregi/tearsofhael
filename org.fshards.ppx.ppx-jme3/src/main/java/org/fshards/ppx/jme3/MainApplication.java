/**
 * Copyright (c) 2016, 4Shards Entertainment Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.fshards.ppx.jme3;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public class MainApplication extends SimpleApplication
{

	private static final String			PPX_CONFIG_ASSETS_FOLDER	= "ppx.config.assetsFolder";
	private Material					mat_terrain;

	private final Queue< CubeGeometry >	geomQueue					= new LinkedBlockingQueue< CubeGeometry >( );
	private Node						terrain;
	private final int					worldSize					= 16;
	private final int					gridSize					= 8;
	private final float					gridScale					= 1 / 32f;

	public static void main( final String[ ] args )
	{
		final MainApplication app = new MainApplication( );
		app.start( );
	}

	@Override
	public void simpleInitApp( )
	{
		final String assetsFolder = System.getenv( MainApplication.PPX_CONFIG_ASSETS_FOLDER ) == null ? System.getProperty( MainApplication.PPX_CONFIG_ASSETS_FOLDER ) : System.getenv( MainApplication.PPX_CONFIG_ASSETS_FOLDER );

		this.assetManager.registerLocator( assetsFolder, FileLocator.class );

		this.flyCam.setMoveSpeed( 20 );
		// this.rootNode.attachChild( SkyFactory.createSky( this.assetManager,
		// "Textures/Sky/Bright/sky_povray.jpg", true ) );
		this.setUpLight( );

		// this.mat_terrain = new Material( this.assetManager,
		// "MatDefs/Terrain/SlopeBasedTerrain.j3md" );
		this.mat_terrain = new Material( this.assetManager, "Common/MatDefs/Terrain/TerrainLighting.j3md" );

		// final Texture texture = this.assetManager.loadTexture(
		// "Textures/Terrain/terrain/grass_rocky_d.jpg" );
		// texture.setWrap( WrapMode.Repeat );
		// final Texture normal = this.assetManager.loadTexture(
		// "Textures/Terrain/terrain/grass_rocky_n.jpg" );
		// normal.setWrap( WrapMode.Repeat );
		// this.mat_terrain.setTexture( "DiffuseMap", texture );
		// this.mat_terrain.setFloat( "DiffuseMap_0_scale", 0.2f );
		// // this.mat_terrain.setTexture( "NormalMap", normal );

		// this.mat_terrain.setBoolean( "isTerrainGrid", true );
		// this.mat_terrain.setBoolean( "useTriPlanarMapping", true );

		this.mat_terrain.setColor( "Diffuse", ColorRGBA.Green );
		this.mat_terrain.setColor( "Ambient", ColorRGBA.White );
		this.mat_terrain.setColor( "Specular", ColorRGBA.Blue );

		this.terrain = new Node( "Terrain" );

		final Thread geomGenerator = new Thread( new TerrainGenerator( this.geomQueue, this.worldSize, this.gridSize, this.gridScale ) );
		geomGenerator.setDaemon( true );
		geomGenerator.start( );

		this.rootNode.attachChild( this.terrain );

	}

	private void setUpLight( )
	{
		// We add light so we see the scene
		final AmbientLight al = new AmbientLight( );
		al.setColor( ColorRGBA.White );
		this.rootNode.addLight( al );

		final DirectionalLight dl = new DirectionalLight( );
		dl.setColor( ColorRGBA.White );
		dl.setDirection( new Vector3f( 2.8f, -2.8f, -2.8f ).normalizeLocal( ) );
		this.rootNode.addLight( dl );
	}

	@Override
	public void simpleUpdate( final float tpf )
	{
		for ( int i = 0; i < 100; i++ )
		{
			final CubeGeometry poll = this.geomQueue.poll( );
			if ( poll == null )
			{
				break;
			}
			poll.setMaterial( this.mat_terrain );
			this.terrain.attachChild( poll );
			poll.setLocalTranslation( poll.getGridIndex( ).mult( this.gridSize ) );
		}
	}

}
