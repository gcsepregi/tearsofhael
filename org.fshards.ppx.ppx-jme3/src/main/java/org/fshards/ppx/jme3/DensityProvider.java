/**
 * Copyright (c) 2016, 4Shards Entertainment Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.fshards.ppx.jme3;

import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;

public class DensityProvider
{

	private static Matrix3f rot[] = new Matrix3f[ ] {
			new Matrix3f( FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1,
					FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1 ),
			new Matrix3f( FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1,
					FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1 ),
			new Matrix3f( FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1,
					FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1 ),
			new Matrix3f( FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1,
					FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1 ),
			new Matrix3f( FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1,
					FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1 ),
			new Matrix3f( FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1,
					FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1 ),
			new Matrix3f( FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1,
					FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1 ),
			new Matrix3f( FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1,
					FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1 ),
			new Matrix3f( FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1,
					FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1, FastMath.rand.nextFloat( ) * 2 - 1 ) };

	public static float density( final float x, final float y, final float z )
	{
		return DensityProvider.density( new Vector3f( x, y, z ) );
	}

	public static float density( Vector3f v )
	{
		v = v.mult( 1 / 32f );

		final Vector3f v_orig = v.clone( );
		float density = 0;
		final float prewarp_str = 25;

		final Vector3f uulf_rand = ShaderUtils.saturate( SamplerUtils.NMQu( v.mult( 0.000718f ) ).mult( 2 ).add( -0.5f, -0.5f, -0.5f ) );
		final Vector3f uulf_rand2 = SamplerUtils.NMQu( v.mult( 0.000632f ) );
		final Vector3f uulf_rand3 = SamplerUtils.NMQu( v.mult( 0.000695f ) );

		final Vector3f ulf_rand = SamplerUtils.NMQs( v.mult( 0.0041f ) ).mult( 0.64f ).add( SamplerUtils.NMQs( v.mult( 0.0041f ).mult( 0.427f ) ).mult( 0.32f ) );

		v = v.add( ulf_rand.mult( prewarp_str ).mult( ShaderUtils.saturate( uulf_rand3.x * 1.4f - 0.3f ) ) );

		final Vector3f[ ] v_rot = new Vector3f[ ] { DensityProvider.rot[ 0 ].mult( v ), DensityProvider.rot[ 1 ].mult( v ), DensityProvider.rot[ 2 ].mult( v ), DensityProvider.rot[ 3 ].mult( v ), DensityProvider.rot[ 4 ].mult( v ),
				DensityProvider.rot[ 5 ].mult( v ), DensityProvider.rot[ 6 ].mult( v ), DensityProvider.rot[ 7 ].mult( v ), DensityProvider.rot[ 8 ].mult( v ) };
		final float hard_floor_y = -6 / 32f;
		density = -v.y;
		density += ShaderUtils.saturate( ( -4 - v_orig.y * 0.3f ) * 0.3f ) * 20;

		final float shelf_thickness_y = 2.5f / 32f;// 2.5;
		final float shelf_pos_y = -1 / 32f;// -2;
		final float shelf_strength = 9.5f / 32f; // 1-4 is good
		density = ShaderUtils.lerp( density, shelf_strength, 0.83f * ShaderUtils.saturate( shelf_thickness_y - ShaderUtils.abs( v.y - shelf_pos_y ) ) ) * ShaderUtils.saturate( uulf_rand.y * 1.5f - 0.5f );
		// saturate(
		// uulf_rand.y
		// *
		// 1.5f
		// -
		// 0.5f
		// )
		// );

		float frequency = 1.0f;
		float amplitude = 1.0f;

		final float roughness = 0.25f;
		final float lacunarity = 1.6513f;

		for ( int i = 0; i < 2; i++ )
		{
			density += ImprovedNoise.noise( v_rot[ i ].mult( frequency ) ) * amplitude;
			frequency *= lacunarity;
			amplitude *= roughness;
		}

		// density += ShaderUtils.clamp( ( hard_floor_y - v.y ) * 3, 0, 1 ) *
		// 40;

		// for ( int i = 0; i < 3; i++ )
		// {
		// density += ImprovedNoise.noise( v.mult( frequency ) ) * amplitude;
		//
		// frequency *= lacunarity;
		// amplitude *= roughness;
		// }
		//
		// System.out.println( density );
		return density;
	}

}
