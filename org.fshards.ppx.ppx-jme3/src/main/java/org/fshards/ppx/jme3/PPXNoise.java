/**
 * Copyright (c) 2016, 4Shards Entertainment Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.fshards.ppx.jme3;

public class PPXNoise
{

	private static float noise( final int x )
	{
		final int xx = x << 13 ^ x;
		return 1f + ( x * ( x * x * 15731 + 789221 ) + 1376312589 & 0x7fffffff ) / 1073741824f;
	}

	private static float noise( final float x, final float y )
	{
		int n = ( int ) ( x * y * 573 );
		n = n << 13 ^ n;
		return 1f - ( n * ( n * n * 15731 + 789221 ) + 1376312589 & 0x7fffffff ) / 1073741824f;
	}

	private static float noise( final float x, final float y, final float z )
	{
		int n = ( int ) ( x * y * z * 573 );
		n = n << 13 ^ n;
		return 1f - ( n * ( n * n * 15731 + 789221 ) + 1376312589 & 0x7fffffff ) / 1073741824f;
	}

	private static float lerp( final float a, final float b, final float x )
	{
		return a * ( 1 - x ) + b * x;
	}

	private static float cosineInterpolate( final float a, final float b, final float x )
	{
		final float ft = x * 3.1415927f;
		final float f = ( 1 - ( float ) Math.cos( ft ) ) * .5f;
		return a * ( 1 - f ) + b * f;
	}

	private static float smoothNoise( final float x, final float y )
	{
		final int xx = ( int ) x;
		final float corners = ( PPXNoise.noise( xx - 1, y - 1 ) + PPXNoise.noise( xx + 1, y - 1 ) + PPXNoise.noise( xx - 1, y + 1 ) + PPXNoise.noise( xx + 1, y + 1 ) ) / 16f;
		final float sides = ( PPXNoise.noise( xx - 1, y ) + PPXNoise.noise( xx + 1, y ) + PPXNoise.noise( xx, y - 1 ) + PPXNoise.noise( xx, y + 1 ) ) / 8f;
		final float center = PPXNoise.noise( xx, y ) / 4f;
		return corners + sides + center;
	}

	private static float smoothNoise( final float x, final float y, final float z )
	{
		final int xx = ( int ) x;
		final float corners = ( PPXNoise.noise( xx - 1, y - 1, z - 1 ) + PPXNoise.noise( xx + 1, y - 1, z - 1 ) + PPXNoise.noise( xx - 1, y + 1, z - 1 ) + PPXNoise.noise( xx + 1, y + 1, z - 1 ) + PPXNoise.noise( xx - 1, y - 1, z + 1 )
				+ PPXNoise.noise( xx + 1, y - 1, z + 1 ) + PPXNoise.noise( xx - 1, y + 1, z + 1 ) + PPXNoise.noise( xx + 1, y + 1, z + 1 ) ) / 32f;
		final float sides1 = ( PPXNoise.noise( xx - 1, y, z ) + PPXNoise.noise( xx + 1, y, z ) + PPXNoise.noise( xx, y - 1, z ) + PPXNoise.noise( xx, y + 1, z ) ) / 24f;
		final float sides2 = ( PPXNoise.noise( xx - 1, y, z - 1 ) + PPXNoise.noise( xx + 1, y, z - 1 ) + PPXNoise.noise( xx, y - 1, z - 1 ) + PPXNoise.noise( xx, y + 1, z - 1 ) ) / 24f;
		final float sides3 = ( PPXNoise.noise( xx - 1, y, z + 1 ) + PPXNoise.noise( xx + 1, y, z + 1 ) + PPXNoise.noise( xx, y - 1, z + 1 ) + PPXNoise.noise( xx, y + 1, z + 1 ) ) / 24f;
		final float center = PPXNoise.noise( x, y ) / 4f;
		return corners + sides1 + sides2 + sides3 + center;
	}

	private static float interpolatedNoise( final float x, final float y )
	{
		final int XX = ( int ) x;
		final float Xfrac = x - XX;
		final int YY = ( int ) y;
		final float Yfrac = y - YY;

		final float v1 = PPXNoise.smoothNoise( XX, YY );
		final float v2 = PPXNoise.smoothNoise( XX + 1, YY );
		final float v3 = PPXNoise.smoothNoise( XX, YY + 1 );
		final float v4 = PPXNoise.smoothNoise( XX + 1, YY + 1 );

		final float i1 = PPXNoise.cosineInterpolate( v1, v2, Xfrac );
		final float i2 = PPXNoise.cosineInterpolate( v3, v4, Xfrac );

		return PPXNoise.cosineInterpolate( i1, i2, Yfrac );
	}

	public static float interpolatedNoise( final float x, final float y, final float z )
	{
		final int XX = ( int ) x;
		final float Xfrac = x - XX;
		final int YY = ( int ) y;
		final float Yfrac = y - YY;
		final int ZZ = ( int ) z;
		final float Zfrac = z - ZZ;

		final float v1 = PPXNoise.smoothNoise( XX, YY, ZZ );
		final float v2 = PPXNoise.smoothNoise( XX + 1, YY, ZZ );
		final float v3 = PPXNoise.smoothNoise( XX, YY + 1, ZZ );
		final float v4 = PPXNoise.smoothNoise( XX + 1, YY + 1, ZZ );
		final float v5 = PPXNoise.smoothNoise( XX, YY, ZZ + 1 );
		final float v6 = PPXNoise.smoothNoise( XX + 1, YY, ZZ + 1 );
		final float v7 = PPXNoise.smoothNoise( XX, YY + 1, ZZ + 1 );
		final float v8 = PPXNoise.smoothNoise( XX + 1, YY + 1, ZZ + 1 );

		final float i1 = PPXNoise.cosineInterpolate( v1, v2, Xfrac );
		final float i2 = PPXNoise.cosineInterpolate( v3, v4, Xfrac );
		final float i3 = PPXNoise.cosineInterpolate( v5, v6, Xfrac );
		final float i4 = PPXNoise.cosineInterpolate( v7, v8, Xfrac );

		final float j1 = PPXNoise.cosineInterpolate( i1, i2, Yfrac );
		final float j2 = PPXNoise.cosineInterpolate( i3, i4, Yfrac );

		return PPXNoise.cosineInterpolate( j1, j2, Zfrac );
	}

}
