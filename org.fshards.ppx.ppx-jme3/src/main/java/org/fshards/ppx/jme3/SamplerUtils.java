/**
 * Copyright (c) 2016, 4Shards Entertainment Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.fshards.ppx.jme3;

import com.jme3.math.Vector3f;

public class SamplerUtils
{
	private static final float NOISE_LATTICE_SIZE = 32;

	public static Vector3f NMQu( final Vector3f uvw )
	{
		// smooth the input coord
		final Vector3f t = SamplerUtils.frac( uvw.mult( SamplerUtils.NOISE_LATTICE_SIZE ).add( 0.5f, 0.5f, 0.5f ) );
		final Vector3f t2 = t.mult( -2 ).add( 3, 3, 3 ).mult( t ).mult( t );
		final Vector3f uvw2 = uvw.add( t2.add( t.mult( -1 ) ).mult( 1f / SamplerUtils.NOISE_LATTICE_SIZE ) );
		// fetch
		return SamplerUtils.NLQu( uvw2 );
	}

	public static Vector3f NMQs( final Vector3f uvw )
	{
		// smooth the input coord
		final Vector3f t = SamplerUtils.frac( uvw.mult( SamplerUtils.NOISE_LATTICE_SIZE ).add( 0.5f, 0.5f, 0.5f ) );
		final Vector3f t2 = t.mult( -2 ).add( 3, 3, 3 ).mult( t ).mult( t );
		final Vector3f uvw2 = uvw.add( t2.add( t.mult( -1 ) ).mult( 1f / SamplerUtils.NOISE_LATTICE_SIZE ) );
		// fetch
		return SamplerUtils.NLQs( uvw2 );
	}

	public static Vector3f NLQu( final Vector3f uvw )
	{
		return new Vector3f( ImprovedNoise.noise( uvw ), ImprovedNoise.noise( uvw ), ImprovedNoise.noise( uvw ) );
	}

	public static Vector3f NLQs( final Vector3f uvw )
	{
		return SamplerUtils.NLQu( uvw ).mult( 2 ).add( -1, -1, -1 );
	}

	private static Vector3f frac( final Vector3f v )
	{
		final float X = ShaderUtils.floor( v.x ), Y = ShaderUtils.floor( v.y ), Z = ShaderUtils.floor( v.z );
		return v.add( -X, -Y, -Z );
	}

}
