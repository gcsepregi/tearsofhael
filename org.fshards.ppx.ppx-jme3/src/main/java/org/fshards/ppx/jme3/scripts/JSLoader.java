package org.fshards.ppx.jme3.scripts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;

import com.jme3.asset.AssetInfo;
import com.jme3.asset.AssetLoader;

public class JSLoader implements AssetLoader
{

	@Override
	public Object load( final AssetInfo arg0 ) throws IOException
	{
		final InputStreamReader streamReader = new InputStreamReader( arg0.openStream( ) );
		final BufferedReader bufferedReader = new BufferedReader( streamReader );
		String line = null;
		final StringWriter sw = new StringWriter( );
		while ( ( line = bufferedReader.readLine( ) ) != null )
		{
			sw.write( line + "\n" );
		}
		return sw.toString( );
	}

}
