/**
 * Copyright (c) 2016, 4Shards Entertainment Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.fshards.ppx.jme3;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.fshards.ppx.mcubes.GridCell;
import org.fshards.ppx.mcubes.MCubes;
import org.fshards.ppx.mcubes.Triangle;

import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.util.BufferUtils;

public class TerrainGenerator implements Runnable
{

	private final Queue< CubeGeometry >	outputQueue;
	private final int					worldSize;
	private final int					gridSize;
	private final float					gridScale;

	public TerrainGenerator( final Queue< CubeGeometry > outputQueue, final int worldSize, final int gridSize, final float gridScale )
	{
		this.outputQueue = outputQueue;
		this.worldSize = worldSize;
		this.gridSize = gridSize;
		this.gridScale = gridScale;
	}

	@Override
	public void run( )
	{
		for ( int i = -this.worldSize; i < this.worldSize; i++ )
		{
			for ( int j = -this.worldSize; j < this.worldSize; j++ )
			{
				for ( int k = -this.worldSize; k < this.worldSize; k++ )
				{
					final CubeGeometry terrainGeo = this.createGeometryAt( new Vector3f( i, j, k ) );
					this.outputQueue.add( terrainGeo );
				}
			}
		}

		System.out.println( "Done creating terrain" );
	}

	private CubeGeometry createGeometryAt( final Vector3f gridIndex )
	{
		final Mesh terrainMesh = new Mesh( );
		final CubeGeometry terrainGeo = new CubeGeometry( "OurMesh" + gridIndex.toString( ), terrainMesh );

		final List< Triangle > triangles = new LinkedList< Triangle >( );

		for ( float i = -this.gridSize / 2f; i < this.gridSize / 2f; i += 1 )
		{
			final float si0 = i + gridIndex.x * this.gridSize;
			final float si1 = i + 1 + gridIndex.x * this.gridSize;

			for ( float j = -this.gridSize / 2f; j < this.gridSize / 2f; j += 1 )
			{
				final float sj0 = j + gridIndex.y * this.gridSize;
				final float sj1 = j + 1 + gridIndex.y * this.gridSize;

				for ( float k = -this.gridSize / 2f; k < this.gridSize / 2f; k += 1 )
				{
					final float sk0 = k + gridIndex.z * this.gridSize;
					final float sk1 = k + 1 + gridIndex.z * this.gridSize;

					final GridCell c = new GridCell( );

					c.center.set( new Vector3f( i + gridIndex.x * this.gridSize + this.gridSize / 2f, j + gridIndex.y * this.gridSize + this.gridSize / 2f, k + gridIndex.z * this.gridSize + this.gridSize / 2f ) );

					c.p[ 0 ] = new Vector3f( i, j, k );
					c.p[ 1 ] = new Vector3f( i + 1, j, k );
					c.p[ 2 ] = new Vector3f( i + 1, j + 1, k );
					c.p[ 3 ] = new Vector3f( i, j + 1, k );
					c.p[ 4 ] = new Vector3f( i, j, k + 1 );
					c.p[ 5 ] = new Vector3f( i + 1, j, k + 1 );
					c.p[ 6 ] = new Vector3f( i + 1, j + 1, k + 1 );
					c.p[ 7 ] = new Vector3f( i, j + 1, k + 1 );

					c.val[ 0 ] = DensityProvider.density( si0, sj0, sk0 );
					c.val[ 1 ] = DensityProvider.density( si1, sj0, sk0 );
					c.val[ 2 ] = DensityProvider.density( si1, sj1, sk0 );
					c.val[ 3 ] = DensityProvider.density( si0, sj1, sk0 );
					c.val[ 4 ] = DensityProvider.density( si0, sj0, sk1 );
					c.val[ 5 ] = DensityProvider.density( si1, sj0, sk1 );
					c.val[ 6 ] = DensityProvider.density( si1, sj1, sk1 );
					c.val[ 7 ] = DensityProvider.density( si0, sj1, sk1 );

					triangles.addAll( MCubes.polygonise( c, 0 ) );
				}
			}
		}

		// System.out.println( gridIndex );
		this.generateTriangleMesh( terrainMesh, triangles, gridIndex );
		terrainMesh.updateBound( );
		terrainMesh.setStatic( );
		terrainGeo.updateModelBound( );
		terrainGeo.setGridIndex( gridIndex );
		return terrainGeo;
	}

	private void generateTriangleMesh( final Mesh terrainMesh, final List< Triangle > triangles, final Vector3f gridIndex )
	{
		final List< Vector3f > verticesList = new LinkedList< Vector3f >( );
		final List< Vector3f > normalsList = new LinkedList< Vector3f >( );

		Vector3f[ ] vertices = new Vector3f[ triangles.size( ) * 3 ];
		final int[ ] indexes = new int[ triangles.size( ) * 3 ];
		Vector3f[ ] normCoord = new Vector3f[ triangles.size( ) * 3 ];

		for ( int j = 0; j < triangles.size( ); j++ )
		{
			final Triangle t = triangles.get( j );
			for ( int i = 0; i < 3; i++ )
			{
				final int idx = verticesList.indexOf( t.p[ i ] );
				if ( idx >= 0 )
				{
					indexes[ j * 3 + i ] = idx;
				}
				else
				{
					verticesList.add( t.p[ i ] );
					normalsList.add( this.calculateNormals( t.p[ i ], gridIndex ) );
					indexes[ j * 3 + i ] = verticesList.size( ) - 1;
				}
			}
		}

		vertices = verticesList.toArray( new Vector3f[ verticesList.size( ) ] );
		normCoord = normalsList.toArray( new Vector3f[ normalsList.size( ) ] );

		// System.out.println( "Vertices: " + Arrays.toString( vertices ) );
		// System.out.println( "Indices: " + Arrays.toString( indexes ) );

		terrainMesh.setBuffer( Type.Position, 3, BufferUtils.createFloatBuffer( vertices ) );
		terrainMesh.setBuffer( Type.Index, 3, BufferUtils.createIntBuffer( indexes ) );
		terrainMesh.setBuffer( Type.Normal, 3, BufferUtils.createFloatBuffer( normCoord ) );
		terrainMesh.setMode( Mesh.Mode.Triangles );
	}

	private void generateLineMesh( final Mesh terrainMesh, final List< Triangle > triangles, final Vector3f gridIndex )
	{
		final List< Vector3f > verticesList = new LinkedList< Vector3f >( );

		Vector3f[ ] vertices = new Vector3f[ triangles.size( ) * 3 ];
		final int[ ] indexes = new int[ triangles.size( ) * 6 ];
		final Vector3f[ ] normCoord = new Vector3f[ triangles.size( ) * 3 ];

		for ( int j = 0; j < triangles.size( ); j++ )
		{
			final Triangle t = triangles.get( j );
			for ( int i = 0; i < 3; i++ )
			{
				final int idx = verticesList.indexOf( t.p[ i ] );
				if ( idx < 0 )
				{
					verticesList.add( t.p[ i ] );
				}
			}

			indexes[ j * 6 ] = verticesList.indexOf( t.p[ 0 ] );
			indexes[ j * 6 + 1 ] = verticesList.indexOf( t.p[ 1 ] );
			indexes[ j * 6 + 2 ] = verticesList.indexOf( t.p[ 1 ] );
			indexes[ j * 6 + 3 ] = verticesList.indexOf( t.p[ 2 ] );
			indexes[ j * 6 + 4 ] = verticesList.indexOf( t.p[ 2 ] );
			indexes[ j * 6 + 5 ] = verticesList.indexOf( t.p[ 0 ] );

			normCoord[ j * 3 ] = this.calculateNormals( t.p[ 0 ], gridIndex );
			normCoord[ j * 3 + 1 ] = this.calculateNormals( t.p[ 1 ], gridIndex );
			normCoord[ j * 3 + 2 ] = this.calculateNormals( t.p[ 2 ], gridIndex );
		}

		vertices = verticesList.toArray( new Vector3f[ verticesList.size( ) ] );

		// System.out.println( "Vertices: " + Arrays.toString( vertices ) );
		// System.out.println( "Indices: " + Arrays.toString( indexes ) );

		terrainMesh.setBuffer( Type.Position, 3, BufferUtils.createFloatBuffer( vertices ) );
		terrainMesh.setBuffer( Type.Index, 3, BufferUtils.createIntBuffer( indexes ) );
		terrainMesh.setBuffer( Type.Normal, 3, BufferUtils.createFloatBuffer( normCoord ) );
		terrainMesh.setMode( Mesh.Mode.Lines );
	}

	private Vector3f calculateNormals( final Vector3f v, final Vector3f gridIndex )
	{
		final float d = this.gridScale / this.gridSize;
		final float sx = v.x + gridIndex.x * this.gridSize;
		final float sy = v.y + gridIndex.y * this.gridSize;
		final float sz = v.z + gridIndex.z * this.gridSize;

		final float nx = DensityProvider.density( sx + d, sy, sz ) - DensityProvider.density( sx, sy, sz );
		final float ny = DensityProvider.density( sx, sy + d, sz ) - DensityProvider.density( sx, sy, sz );
		final float nz = DensityProvider.density( sx, sy, sz + d ) - DensityProvider.density( sx, sy, sz );

		final Vector3f normal = new Vector3f( nx, ny, nz ).normalize( ).mult( -1 );

		// System.out.println( MessageFormat.format( "Position: {0} -
		// {1}/{2}/{3} | {4} | {5}/{6}/{7} | {8}", v, sx, sy, sz, d, nx, ny, nz,
		// normal ) );

		return normal;
	}

}
