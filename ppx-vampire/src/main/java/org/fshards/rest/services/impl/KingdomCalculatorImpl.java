/**
 * Copyright (c) 2016, 4Shards Entertainment Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.fshards.rest.services.impl;

import java.security.SecureRandom;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import org.fshards.rest.beans.KingdomStats;
import org.fshards.rest.beans.Settlement;
import org.fshards.rest.services.KingdomCalculator;
import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class KingdomCalculatorImpl implements KingdomCalculator
{

	private static final Logger logger = LoggerFactory.getLogger( KingdomCalculatorImpl.class );

	@Override
	public KingdomStats getPopulationStats( final String kingdomId, final int kingdomSize, final int populationDensity )
	{
		final SecureRandom rnd = new SecureRandom( kingdomId.getBytes( ) );
		final KingdomStats stats = new KingdomStats( );
		stats.setKingdomSize( kingdomSize );
		stats.setPopulationDensity( populationDensity );
		stats.setDomainPopulation( kingdomSize * populationDensity );

		int populationFound = this.generateCities( rnd, stats );
		populationFound = this.generateTowns( rnd, stats, populationFound );

		KingdomCalculatorImpl.logger.info( "Population found so far in urban environment: {}", populationFound );

		// stats.setCity( Math.round( stats.getDomain( ) * 0.03f > 8000 ?
		// stats.getDomain( ) * 0.03f : 0 ) );
		// stats.setTown( Math.round( stats.getDomain( ) * 0.06f > 1000 ?
		// stats.getDomain( ) * 0.06f : 0 ) );
		stats.setVillage( Math.round( stats.getDomainPopulation( ) * 0.89f > 30 ? stats.getDomainPopulation( ) * 0.89f : 0 ) );
		// stats.setOther( stats.getDomain( ) - stats.getCity( ) -
		// stats.getTown( ) - stats.getVillage( ) );
		return stats;
	}

	private int generateTowns( final SecureRandom rnd, final KingdomStats stats, final int populationFound )
	{
		int populationFoundLocal = populationFound;
		final int numberOfTowns = stats.getCities( ).size( ) * ( rnd.nextInt( 15 ) + 1 );

		for ( int i = 0, nextTown = rnd.nextInt( 7000 ) + 1000; i < numberOfTowns && populationFound + nextTown < stats.getDomainPopulation( ); i++, nextTown = rnd.nextInt( 7000 ) + 1000 )
		{
			stats.getTowns( ).add( ( long ) nextTown );
			populationFoundLocal += nextTown;
		}
		stats.getTowns( ).sort( new Comparator< Long >( )
		{
			@Override
			public int compare( final Long o1, final Long o2 )
			{
				return o2.compareTo( o1 );
			}
		} );
		return populationFoundLocal;
	}

	private int generateCities( final SecureRandom rnd, final KingdomStats stats )
	{
		int populationFound = 0;
		/**
		 * Calculate city population if domain population is larger than 8000
		 *
		 * The number 445 * 445 ((8000 / 18) ^ 2) is the minimal number that
		 * makes the first calculation give positive result
		 */
		if ( stats.getDomainPopulation( ) > 445 * 445 )
		{
			final long biggestCity = Math.round( Math.sqrt( stats.getDomainPopulation( ) ) * 15 );
			// ( rnd.nextInt( 6 ) + 12 ) );
			stats.getCities( ).add( biggestCity );
			populationFound += biggestCity;
			long next = Math.round( stats.getCities( ).get( stats.getCities( ).size( ) - 1 ) * 50f / 100f );
			if ( next > 8000 && populationFound < stats.getDomainPopulation( ) )
			{
				stats.getCities( ).add( next );
				populationFound += next;
			}
			next = Math.round( next * 50f / 100f );
			KingdomCalculatorImpl.logger.info( "Next city found with population of: {}", next );
			while ( next > 8000 && populationFound < stats.getDomainPopulation( ) )
			{
				stats.getCities( ).add( next );
				populationFound += next;
				next = Math.round( next * 50f / 100f );
				KingdomCalculatorImpl.logger.info( "Next city found with population of: {}", next );
			}
		}
		return populationFound;
	}

	@Override
	public Set< Settlement > getSettlements( final int populationTotal, final int populationMin, final int populationMax, final int seed )
	{
		final Set< Settlement > settlements = new HashSet< Settlement >( );
		int remaining = populationTotal;
		final SecureRandom random = new SecureRandom( String.valueOf( seed ).getBytes( ) );

		while ( remaining > populationMin )
		{
			final int nextSettlement = Math.round( random.nextFloat( ) * ( populationMax - populationMin ) + populationMin );
			final Settlement settlement = new Settlement( );
			settlement.setPopulation( nextSettlement );
			settlements.add( settlement );
			remaining -= nextSettlement;
		}

		return settlements;
	}

}
