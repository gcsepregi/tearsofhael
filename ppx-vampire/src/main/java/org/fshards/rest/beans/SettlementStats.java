/**
 * Copyright (c) 2016, 4Shards Entertainment Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.fshards.rest.beans;

import java.util.HashSet;
import java.util.Set;

public class SettlementStats
{

	private Set< Settlement >	cities;

	private Set< Settlement >	towns;

	private Set< Settlement >	villages;

	public void addSettlement( final Settlement settlement )
	{
		if ( settlement.getPopulation( ) > 8000 )
		{
			this.cities.add( settlement );
		}
		else if ( settlement.getPopulation( ) > 1000 )
		{
			this.towns.add( settlement );
		}
		else
		{
			this.villages.add( settlement );
		}
	}

	public Set< Settlement > getCities( )
	{
		if ( this.cities == null )
		{
			this.cities = new HashSet< Settlement >( );
		}
		return this.cities;
	}

	public void setCities( final Set< Settlement > cities )
	{
		this.cities = cities;
	}

	public Set< Settlement > getTowns( )
	{
		if ( this.towns == null )
		{
			this.towns = new HashSet< Settlement >( );
		}
		return this.towns;
	}

	public void setTowns( final Set< Settlement > towns )
	{
		this.towns = towns;
	}

	public Set< Settlement > getVillages( )
	{
		if ( this.villages == null )
		{
			this.villages = new HashSet< Settlement >( );
		}
		return this.villages;
	}

	public void setVillages( final Set< Settlement > villages )
	{
		this.villages = villages;
	}

	@Override
	public String toString( )
	{
		return "SettlementStats [cities=" + this.cities + ", towns=" + this.towns + ", villages=" + this.villages + "]";
	}

}
