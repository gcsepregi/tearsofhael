/**
 * Copyright (c) 2016, 4Shards Entertainment Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.fshards.rest.beans;

import java.util.ArrayList;
import java.util.List;

public class KingdomStats
{

	private int				kingdomSize;

	private int				populationDensity;

	private long			domainPopulation;

	private List< Long >	cities	= new ArrayList< Long >( );

	private List< Long >	towns	= new ArrayList< Long >( );

	private long			village;

	private long			other;

	public long getDomainPopulation( )
	{
		return this.domainPopulation;
	}

	public void setDomainPopulation( final long domainPopulation )
	{
		this.domainPopulation = domainPopulation;
	}

	public List< Long > getCities( )
	{
		return this.cities;
	}

	public void setCities( final List< Long > cities )
	{
		this.cities = cities;
	}

	public List< Long > getTowns( )
	{
		return this.towns;
	}

	public void setTowns( final List< Long > towns )
	{
		this.towns = towns;
	}

	public long getVillage( )
	{
		return this.village;
	}

	public void setVillage( final long village )
	{
		this.village = village;
	}

	public long getOther( )
	{
		return this.other;
	}

	public void setOther( final long other )
	{
		this.other = other;
	}

	public long getKingdomSize( )
	{
		return this.kingdomSize;
	}

	public void setKingdomSize( final int kingdomSize )
	{
		this.kingdomSize = kingdomSize;
	}

	public int getPopulationDensity( )
	{
		return this.populationDensity;
	}

	public void setPopulationDensity( final int populationDensity )
	{
		this.populationDensity = populationDensity;
	}

	@Override
	public String toString( )
	{
		return "KingdomStats [kingdomSize=" + this.kingdomSize + ", populationDensity=" + this.populationDensity + ", domain=" + this.domainPopulation + ", city=" + this.cities + ", town=" + this.towns + ", village=" + this.village + ", other="
				+ this.other + "]";
	}

	@Override
	public int hashCode( )
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ( this.cities == null ? 0 : this.cities.hashCode( ) );
		result = prime * result + ( int ) ( this.domainPopulation ^ this.domainPopulation >>> 32 );
		result = prime * result + this.kingdomSize;
		result = prime * result + ( int ) ( this.other ^ this.other >>> 32 );
		result = prime * result + this.populationDensity;
		result = prime * result + ( this.towns == null ? 0 : this.towns.hashCode( ) );
		result = prime * result + ( int ) ( this.village ^ this.village >>> 32 );
		return result;
	}

	@Override
	public boolean equals( final Object obj )
	{
		if ( this == obj )
		{
			return true;
		}
		if ( obj == null )
		{
			return false;
		}
		if ( this.getClass( ) != obj.getClass( ) )
		{
			return false;
		}
		final KingdomStats other = ( KingdomStats ) obj;
		if ( this.cities == null )
		{
			if ( other.cities != null )
			{
				return false;
			}
		}
		else if ( !this.cities.equals( other.cities ) )
		{
			return false;
		}
		if ( this.domainPopulation != other.domainPopulation )
		{
			return false;
		}
		if ( this.kingdomSize != other.kingdomSize )
		{
			return false;
		}
		if ( this.other != other.other )
		{
			return false;
		}
		if ( this.populationDensity != other.populationDensity )
		{
			return false;
		}
		if ( this.towns == null )
		{
			if ( other.towns != null )
			{
				return false;
			}
		}
		else if ( !this.towns.equals( other.towns ) )
		{
			return false;
		}
		if ( this.village != other.village )
		{
			return false;
		}
		return true;
	}

}
