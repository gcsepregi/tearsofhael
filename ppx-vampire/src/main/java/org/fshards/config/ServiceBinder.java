package org.fshards.config;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletContext;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.jvnet.hk2.annotations.Contract;
import org.jvnet.hk2.annotations.Service;
import org.scannotation.AnnotationDB;
import org.scannotation.WarUrlFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ServiceBinder extends AbstractBinder
{
	private final ServletContext	sc;

	private static final Logger		log	= LoggerFactory.getLogger( MyApplication.class );

	public ServiceBinder( final ServletContext sc )
	{
		this.sc = sc;
	}

	@Override
	protected void configure( )
	{
		final AnnotationDB adb = new AnnotationDB( );
		try
		{
			adb.addIgnoredPackages( "org.glassfish.hk2", "org.jvnet.hk2" );
			adb.scanArchives( WarUrlFinder.findWebInfLibClasspaths( this.sc ) );
			adb.scanArchives( WarUrlFinder.findWebInfClassesPath( this.sc ) );

			final Set< String > services = adb.getAnnotationIndex( ).get( Service.class.getCanonicalName( ) );

			for ( final String service : services )
			{
				final Class< ? > serviceCls = Thread.currentThread( ).getContextClassLoader( ).loadClass( service );
				final Class< ? >[ ] serviceInterFaces = serviceCls.getInterfaces( );
				for ( final Class< ? > interfc : serviceInterFaces )
				{
					if ( interfc.isAnnotationPresent( Contract.class ) )
					{
						ServiceBinder.log.info( "Binding service class: [" + service + "] to contract interface: [" + interfc.getCanonicalName( ) + "]" );
						this.bind( serviceCls ).to( interfc );
					}
				}
			}

		}
		catch ( final IOException e )
		{
			e.printStackTrace( );
		}
		catch ( final ClassNotFoundException e )
		{
			e.printStackTrace( );
		}
	}
}
