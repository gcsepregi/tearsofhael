package org.fshards.config;

import javax.servlet.ServletContext;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Context;

import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationPath( "api" )
public class MyApplication extends ResourceConfig
{

	private static final Logger log = LoggerFactory.getLogger( MyApplication.class );

	public MyApplication( @Context final ServletContext sc )
	{
		this.packages( "org.fshards.rest.api", "io.swagger.jaxrs.listing" );
		this.register( new ServiceBinder( sc ) );
	}

}