<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="js/knockout-3.4.0.js"></script>
<title>Kingdom generator</title>
</head>
<body>
	<div id="body">
		<h1>Kingdom generator</h1>
		<form action="<%=request.getContextPath(  ) %>/calculation.do">

			<label for="kingdomSize">Size of the kingdom</label> <input
				type="text" name="kingdomSize" id="kingdomSize"
				data-bind="value: kingdomSize" /> square kms <br /> <label
				for="populationDensity">Population density</label> <input
				type="text" name="populationDensity" id="populationDensity"
				data-bind="value: populationDensity" /> /sq km (12-48 [6d4*2]) <br />

		</form>
		<div>
			Total population: <span data-bind="text: population">todo</span>
		</div>

		<canvas id="gamescreen" width="500" height="500"
			style="border: 1px solid #000000;"></canvas>

	</div>
</body>
<script type="text/javascript">
	function AppViewModel() {
		this.kingdomSize = ko.observable(70000);
		this.populationDensity = ko.observable(30);
		this.population = ko.computed(function() {
			return this.kingdomSize() * this.populationDensity();
		}, this);
	}
	
	ko.applyBindings(new AppViewModel());
	
	$( document ).ready( function () {
		var points = [];
		for (var i=0; i<20; i++) {
			points.push({x: Math.random() * 480 + 10, y: Math.random() * 480 + 10});
		}
		
		var c = document.getElementById("gamescreen");
		var ctx = c.getContext("2d");
		ctx.fillStyle = "#FF0000";
		for (var i=0; i<points.length; i++) {
			ctx.beginPath();
			ctx.arc(points[i].x,points[i].y,2,0,2*Math.PI);
			ctx.stroke();
		}
	});
</script>
</html>
