function AppViewModel() {
	var self = this;
    this.kingdomSize = ko.observable(70000);
    this.kingdomAge = ko.observable(300);
    this.populationDensity = ko.observable(71);
    this.agriculturalQuality = ko.observable(1);
    
    this.population = new PopulationStats(this.kingdomSize, this.populationDensity);
    this.fortifications = new FortificationStats(this.population.domain, this.kingdomAge);
	this.agriculture = new AgriculturalStats(this.population.domain, this.agriculturalQuality, this.kingdomSize);
	this.settlements = new SettlementStats(this.population.city, this.population.town);
}

function PopulationStats(kingdomSize, populationDensity) {
    this.domain = ko.computed(function() {
    	return kingdomSize() * populationDensity();
    }, this);
    this.city = ko.computed(function() {
		return Math.floor(this.domain() * 0.03);
	}, this);
	this.town = ko.computed(function() {
		return Math.floor(this.domain() * 0.06);
	}, this);
	this.village = ko.computed(function() {
		return Math.floor(this.domain() * 0.89);
	}, this);
	this.other = ko.computed(function() {
		return Math.floor(this.domain() - this.city() - this.town() - this.village());
	}, this);
}

function FortificationStats(domain, kingdomAge) {
	this.ruined = ko.computed(function() {
		return Math.floor(domain() / 5000000 * Math.sqrt(kingdomAge()));
	}, this);
	this.active = ko.computed(function() {
		return Math.floor(domain() / 50000);
	}, this);
	this.civilized = ko.computed(function() {
		return Math.floor(this.active() * 0.75);
	}, this);
	this.wilderness = ko.computed(function() {
		return Math.floor(this.active() - this.civilized());
	}, this);
	this.total = ko.computed(function() {
		return this.ruined() + this.active();
	}, this);
}

function AgriculturalStats(domain, agriculturalQuality, kingdomSize) {
	this.activeFarmland = ko.computed(function() {
		return Math.floor(domain() / (agriculturalQuality() * 180));
	}, this);
	this.activeFarmlandPercentage = ko.computed(function() {
		return Math.floor(this.activeFarmland() * 100 / kingdomSize());
	}, this);
	this.livestock = ko.computed(function() {
		return Math.floor(domain() * 2.2);
	}, this);
	this.poultry = ko.computed(function() {
		return Math.floor(this.livestock() * 0.68);
	}, this);
	this.dairyMeat = ko.computed(function() {
		return Math.round(this.livestock() - this.poultry());
	}, this);
}

function roll(count, sides, modifier) {
	var result = modifier;
	for (var i=0; i<count; i++) {
		result += Math.round((Math.random() * sides)) + 1;
	}
	return result;
}

function SettlementStats(cities, towns) {
	this.cities = ko.computed(function() {
		var result = ko.observableArray();
		var remaining = cities();
		while (remaining > 8000) {
			var pop = Math.floor(Math.random() * 70000 + 8000);
			result.push(new City(pop));
			remaining -= pop;
		}
		result()[result().length-1].population += remaining;
		result.sort(function(left, right) {
			return left.population == right.population ? 0 : left.population < right.population ? 1 : -1;
		});
		return result;
	});
	
	this.towns = ko.computed(function() {
		var result = ko.observableArray();
		var remaining = towns();
		while (remaining > 1000) {
			var pop = Math.floor(Math.random() * 7000 + 1000);
			result.push(new Town(pop));
			remaining -= pop;
		}
		result()[result().length-1].population += remaining;
		result.sort(function(left, right) {
			return left.population == right.population ? 0 : left.population < right.population ? 1 : -1;
		});
		return result;
	});
	this.villages = ko.observableArray();
}

function City(population) {
	this.population = population;
	this.name = generateName(Math.floor(Math.random()*4) + 3);
}

function Town(population) {
	this.population = population;
	this.name = generateName(Math.floor(Math.random()*4) + 3);
}

function generateName(length) {
	var start = new Array("Kr", "Ca", "Ra", "Mrok", "Cru",
	                       "Ray", "Bre", "Zed", "Drak", "Mor", "Jag", "Mer", "Jar", "Mjol",
	                       "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro",
	                       "Mar", "Luk");
	var middle = new Array( "air", "ir", "mi", "sor", "mee", "clo",
	                         "red", "cra", "ark", "arc", "miri", "lori", "cres", "mur", "zer",
	                         "marac", "zoir", "slamar", "salmar", "urak");
	var end = new Array("d", "ed", "ark", "arc", "es", "er", "der",
	                     "tron", "med", "ure", "zur", "cred", "mur");
			 
	return start[Math.floor(Math.random() * start.length)] + middle[Math.floor(Math.random() * middle.length)] + end[Math.floor(Math.random() * end.length)];
}