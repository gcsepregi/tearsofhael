/**
 * Copyright (c) 2016, 4Shards Entertainment Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package gabor.csepregi.curriculum.vitae;

import java.util.Date;

public abstract class Person
{

	private String	name;
	private Female	mother;
	private Male	father;
	private String	birthPlace;
	private Date	birthDate;

	public void setName( final String string )
	{
		this.name = string;
	}

	public String getName( )
	{
		return this.name;
	}

	public void setMother( final Female mother )
	{
		this.mother = mother;
	}

	public Female getMother( )
	{
		return this.mother;
	}

	public void setFather( final Male father )
	{
		this.father = father;
	}

	public Male getFather( )
	{
		return this.father;
	}

	public void wasBornIn( final String birthPlace )
	{
		this.birthPlace = birthPlace;
	}

	public String getBirthPlace( )
	{
		return this.birthPlace;
	}

	public void wasBornAt( final Date birthDate )
	{
		this.birthDate = birthDate;
	}

	public Date getBirthDate( )
	{
		return this.birthDate;
	}
}
